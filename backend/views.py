from rest_framework.views import APIView
from rest_framework.response import Response
from .models import LogBook
from .serializers import LogBookSerializer
from .validator import check_formula

class CreateLogView(APIView):

    def post(self, request):
        formula = request.data.get('formula')
        if not formula:
            return Response('Введите формулу')
        ip_address= request.META.get('REMOTE_ADDR')
        if not ip_address:
            ip_address = 'Неизвестен'
        result = check_formula(formula)
        query = LogBook.objects.create(data=formula, ip_address=ip_address, result=result)
        serializer = LogBookSerializer(query)
        return Response(serializer.data)
