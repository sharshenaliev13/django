from django.contrib import admin
from .models import LogBook


@admin.register(LogBook)
class LogAdmin(admin.ModelAdmin):
    readonly_fields = ('date', 'ip_address', 'data', 'result')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
