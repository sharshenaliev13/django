from django.urls import path
from .views import CreateLogView


urlpatterns = [
    path('check/', CreateLogView.as_view()),
]