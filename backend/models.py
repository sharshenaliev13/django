from django.db import models


class LogBook(models.Model):
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время запроса')
    ip_address = models.CharField(max_length=100, editable = False, verbose_name='Источник запроса (IP адрес клиента)')
    data = models.CharField(max_length=100, editable = False, verbose_name='Входные данные запроса')
    result = models.BooleanField(editable = False, verbose_name='Результат проверки')

    class Meta:
        verbose_name = 'Журнал'
        verbose_name_plural = 'Журнал'

    def __str__(self):
        return self.data
