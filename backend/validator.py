
def check_formula(formula):
	brackets = []
	for char in formula:
		if char in ["(", "{", "["]:
			brackets.append(char)
		else:
			if not brackets:
				return False
			current_char = brackets.pop()
			match current_char:
				case '(':
					if char != ")":
						return False
				case '{':
					if char != "}":
						return False
				case '[':
					if char != "]":
						return False
	if brackets:
		return False
	return True
