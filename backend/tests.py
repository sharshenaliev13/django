import factory
from rest_framework.test import APITestCase
from rest_framework.status import HTTP_200_OK
from .models import LogBook
from .validator import check_formula


class LogFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LogBook

    ip_address = '127.0.0.1'
    data = factory.Faker('random_element', elements=["()", "(}", "{]"])
    result = check_formula(str(data))


class TestUser(APITestCase):

    def test(self):
        log = LogFactory()
        hour = int(log.date.strftime("%H"))
        hour += 3
        expected_data = {
            "date": log.date.strftime(f"%Y-%m-%d {hour}:%M:%S"),
            "ip_address": log.ip_address,
            "data": log.data,
            "result": log.result
        }
        response = self.client.post("/check/", {'formula': log.data})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)
